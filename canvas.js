$(document).ready(function() {

    var cvs=document.getElementById("canvas");
    var ctx=cvs.getContext("2d");
    var tool="brush";
    var down=false;

    var italic=false;
    var bold=false;
    
    var cPushArray = new Array();
    var cStep = -1;

    function cPush() {
        cStep++;
        if (cStep < cPushArray.length) { cPushArray.length = cStep; }
        cPushArray.push(document.getElementById('canvas').toDataURL());
    }
    

    ctx.strokeStyle="black";
    ctx.lineWidth=1;

    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, 800, 400);

    var size=document.getElementById('size');
    var color=document.getElementById('color');
    //var img=document.getElementById('img');
    var img = new Image;
    size.addEventListener("input", function (e) {        
        ctx.lineWidth=size.value;
    });
    color.addEventListener("input", function (e) {        
        if(tool!='eraser')ctx.strokeStyle=color.value;
    });

    document.getElementById('img').addEventListener("change",function(e){ 
        img.src = URL.createObjectURL(e.target.files[0]);
    });
      
    cPush();

    $('#brush').click(function(){
        tool="brush";
        $('body').css("cursor","url(./img/brush_cur.png)16 32,auto");
        ctx.strokeStyle=color.value;
    });

    $('#eraser').click(function(){
        tool="eraser";
        $('body').css("cursor","url(./img/eraser_cur.png)0 32,auto");
        ctx.strokeStyle="white";
    });
    $('#line').click(function(){
        tool="line";
        $('body').css("cursor","url(./img/line_cur.png),auto");
        ctx.strokeStyle=color.value;
    });
    $('#round').click(function(){
        tool="round";
        $('body').css("cursor","url(./img/round_cur.png),auto");
        ctx.strokeStyle=color.value;
    });
    $('#image').click(function(){
        tool="image";
        $('body').css("cursor","url(./img/image_cur.png),auto");
    });
    $('#text').click(function(){
        tool="text";
        $('body').css("cursor","url(./img/text_cur.png)32 16,auto");
        ctx.strokeStyle=color.value;
    });
    $('#undo').click(function(){
        if (cStep > 0) {
            cStep--;
            var canvasPic = new Image();
            canvasPic.src = cPushArray[cStep];
            canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
        }
    });
    $('#redo').click(function(){
        if (cStep < cPushArray.length-1) {
            cStep++;
            var canvasPic = new Image();
            canvasPic.src = cPushArray[cStep];
            canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
        }
    });
    $('#refresh').click(function(){
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, 800, 400);
        cPush();
    });
    $('#_download').click(function(){
        var _url = cvs.toDataURL();//利用toDataURL() 把canvas轉成data:image
        this.href = _url;//再把href載入上面的Data:image
    });

    $('#square').click(function(){
        $(this).addClass('selected');
        $('#circle').removeClass('selected');
        ctx.lineCap = 'butt';
        ctx.lineJoin = "miter";
    });
    $('#circle').click(function(){
        $(this).addClass('selected');
        $('#square').removeClass('selected');
        ctx.lineCap = 'round';
        ctx.lineJoin = "round";
    });
    $('#italic').click(function(){
        if(italic==true){
            $(this).removeClass('selected');
            italic=false;
        } 
        else{
            $(this).addClass('selected');
            italic=true;
        } 
    });
    $('#bold').click(function(){
        if(bold==true){
            $(this).removeClass('selected');
            bold=false;
        } 
        else{
            $(this).addClass('selected');
            bold=true;
        } 
    });

    $('#canvas').mousedown(function (e) {

        x = e.clientX - canvas.offsetLeft;
        y = e.clientY - canvas.offsetTop;
        if(tool == "brush"||tool == "eraser"||tool=="line")
        {
            down = true;
            ctx.beginPath();
            ctx.moveTo(x,y);
        }  
        if(tool=="round"){
            down = true;
            ctx.beginPath();
        }
        if(tool=='text'){
            var font_=$("input[name=font]:checked").val();

            var font_s='';
            if(italic==true) font_s+='italic ';
            if(bold==true) font_s+='bold ';
            ctx.font=font_s+(size.value*2+10)+"px "+font_;
            ctx.fillStyle = color.value;

            var text_content = prompt("Please enter your text:\n(You could change font size & color & style through the menu :D)", "Hello World!");

            if(text_content!=null){
                ctx.fillText(text_content,x,y);
                cPush();
            }
        }
        if(tool=='image'){
            ctx.drawImage(img, x, y);
            cPush();
        }
        
    });
    $('#canvas').mousemove(function (e) {

        if ( (tool=="brush"||tool == "eraser") && down==true) {
            x = e.clientX - canvas.offsetLeft;
            y = e.clientY - canvas.offsetTop;
            ctx.lineTo(x,y);
            ctx.stroke();
        }
    });
    $('#canvas').mouseup(function (e) {
        down =false;
        _x = e.clientX - canvas.offsetLeft;
        _y = e.clientY - canvas.offsetTop;
        if(tool=="line"){
            ctx.lineTo(_x,_y);
            ctx.stroke();
        }
        if(tool=="round"){
            var R=Math.sqrt(Math.pow(x - _x, 2) + Math.pow(y - _y, 2))/2;
            ctx.arc( (x+_x)/2 , (y+_y)/2 , R ,0,2*Math.PI);
            ctx.stroke();
        }
        cPush();
    });

});
